room = int(input())
if (room % 8) == 0:
    numb = 8
    pod = room // 8
else:
    numb = room % 8
    pod = (room - 1) // 8 + 1
print(pod, numb)

#Задание 2:
a = int(input())
b = int(input())
print(a + b)

#Задание 3:
#X будет равен 12

#Задание 4:
# Получится 72

#Задание 5:
# tomatotomatocucmber

#Задание 6:
s = "change me"
s = s[0:5] + s[5].upper() + s[6] + s[7] + s[8].upper()
print(s)

#Задание 7:
# str_1 + str_2(конкатанация) - происходит "приклеивание" слов друг к другу, т.е. получим redwhite
# "_".join([str_1, str_2]) соединяет строки, а также ставит разделитель между ними, который мы указали в начале
# str_3.find("a") выдает индекс символа в строке

#Задание 8:
values = [7.713, 0.208, 6.336, 7.488, 4.985, 2.248, 1.981, 7.605, 1.691, 0.883, 6.854, 9.534, 0.039, 5.122,
8.126, 6.125, 7.218, 2.919, 9.178, 7.146, 5.425, 1.422, 3.733, 6.741, 4.418]

max_value = max(values[:10])
max_index = values[:10].index(max_value)
print(max_index)

#Задание 9: 
x = [2, 4, 6, 8, 10, 12]
print(x[-1:2:-2]) # [12, 8]
print(x[::-2]) # [12, 8, 4]
print(x[0::0]) # не может быть шаг равным нулю
print(x[1::3]) # [4, 10]
print(x[0]) # [2]

#Задание 10:
a = (2, '1', 1, 10, 1)
print(a.index(1)) # будет вывыведено номер первого вхождения такого элемента, в нашем случае 1

#Задание 11:
element1 = input()
element2 = input()

melt = {'Sn': 232, 'Zn': 420, 'Fe': 1539, 'Ni': 
1455,'Si': 1415, 'Be': 1287}

temp_diff = melt[element1] - melt[element2]
print(temp_diff)

#Задание 12:
s1 = {'Рентген', 'Лоренц', 'Зееман', 'Кюри', 'Милликен', 'Сигбан', 'Франк', 'Герц'}
s2 = {'Фишер', 'Резерфорд', 'Кюри', 'Прегль'}

intersected_scientists = s1.intersection(s2)
print(intersected_scientists)

#Задание 13:
if 2**2 > 4:
    print('yes') #эта программа ничего не будет выводит, т.к. условие не выполненно

#Задание 14:
if 10 > 100:
    print('yes')
else:
    print('nope') #эта программа выведет 'nope'

#Задание 15:
a = 4 
if a/2 > 0:
    print('1')
elif a==4:
    print('2')
elif a < 0:
    print('3')
else: 
    print('4') #ответ: '1'

#Задание 16:
x, y = int(input()), int(input())
if ( x - 1 )**2 + y**2 < 4 and ( x - 1 )**2 + y**2 > 1 and abs( x  - 4 ) < 2 and abs( y - 2 ) < 3:
    print('yes yes')
elif ( x - 1 )**2 + y**2 < 4 and ( x - 1 )**2 + y**2 > 1 and abs( x  - 4 ) >= 2 and abs( y - 2 ) >= 3:
    print('yes no')
elif ( x - 1 )**2 + y**2 >= 4 and ( x - 1 )**2 + y**2 <= 1 and abs( x  - 4 ) < 2 and abs( y - 2 ) < 3:
    print('no yes')
else:
    print('no no')

#Задание 17:
s = input()
x = 0
y = 0
if s[0] == 'a':
    x = 1
    y = int(s[1])
if s[0] == 'b':
    x = 2
    y = int(s[1])
if s[0] == 'c':
    x = 3
    y = int(s[1])
if s[0] == 'd':
    x = 4
    y = int(s[1])
if s[0] == 'e':
    x = 5
    y = int(s[1])
if s[0] == 'f':
    x = 6
    y = int(s[1])
if s[0] == 'g':
    x = 7
    y = int(s[1])
if s[0] == 'h':
    x = 8
    y = int(s[1])
if x % 2 == y % 2:
    print('black')
else:
    print('white')

#Задание 18:
for i in range(3, 11, 4):
 print(i) #ответ: 3 7

#Задание 19:
lst = [2, 6, 43, 1, 66]
s = 0
for item in lst:
    if item % 2 == 0:
        s += 1
    else:
        continue #ответ: 3

#Задание 20:
import math

def ln_one_plus_x(x):
    result = 0
    term = x
    i = 2

    while abs(term) >= 1e-6:
        result += term
        term = (-1) ** (i - 1) * x ** i / i
        i += 1
    return round(result, 8) 

x = float(input())  
result = ln_one_plus_x(x)
print(result)

#Задание 21:
def speed_atom(x):
    max_speed = 0
    
    for i in range(1, len(x)):
        velocity = (x[i] - x[i-1]) / 0.01  
        if abs(velocity) > max_speed:
            max_speed = abs(velocity)
    
    return max_speed

x = list(map(int, input().split()))

max_speed = speed_atom(x)
print(max_speed)

#Задание 23:

def my_filter(x):
    multiplied_list = [str(num * 10) for num in x]
    result = ' '.join(multiplied_list)
    return result

x = list(map(int, input().split()))
numb = my_filter(x)
print(numb)

#Задание 24:
import math
def trapez(func, a, b, N):
    h = (b - a) / N
    result = 0
    
    for i in range(N):
        x = a + i * h
        x1 = a + (i + 1) * h
        result += (func(x) + func(x1)) * h / 2
    
    return round(result, 8)
x = list(input().split())
if x[0] == 'math.sin':
    func = math.sin
elif x[0] == 'math.cos':
    func = math.cos
if x[0] == 'math.tan':
    func = math.tan

x[1] = int(x[1])
x[2] = int(x[2])
x[3] = int(x[3])    
print(trapez(func, x[1], x[2], x[3]))

#Задание 25
def function(a = 1, b = 2, c = 3):
 return int(a + b / c)
x = function(2, c = 1, b = 2) #ответ: 5

#Задание 26
def func(*args):
 lst = []
 for item in args:
    if item % 2 == 0:
        lst.append(item)
 return lst
a, *b, c = func(1, 2, 3, 4, 5, 6, 7, 8)
print(b) #ответ: b = [4, 6], при этом a = 2, c = 8

#Задание 27
def volume(a, b, c):
    if a != undefined and b != undefined and c == undefined:
        S = a * b
        return S
    else:
        V = a * b * c
        return V

x = list(map(int, input().split()))
result = volume(x[0], x[1], x[2])
print(result)
