from CommonUtils import CommonUtils
class SlugConverter:
    def __init__(self):
        self.file_name = slug
        self.slug_list = []
        self.run()

    def run(self):
        while True:
            full_name = input("Введите полное имя человека (для выхода нажмите Enter без ввода): ")
            if full_name == '':
                break

            slug_name = CommonUtils.translit_to_eng(full_name)
            self.slug_list.append(slug_name)

        CommonUtils.save_to_file(self.file_name, self.slug_list)
        print("Результаты сохранены в файл.")


if __name__ == '__main__':
    converter = SlugConverter()