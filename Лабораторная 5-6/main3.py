#Задание 1
class C:
    pass

obj = C()

print(type(obj))

#Задание 2
class Dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
    def get_info(self):
        if self.age > 1 and self.age < 5:
            print(f"Привет! Мою собаку зовут {self.name} - и ей {self.age} года")
        elif self.age >= 5:
            print(f"Привет! Мою собаку зовут {self.name} - и ей {self.age} лет")
        else:
            print(f"Привет! Мою собаку зовут {self.name} - и ей {self.age} год")
    
    def sit(self):
        print(f'{self.name} знает команду "сидеть"')
    
    def roll_over(self):
        print(f'{self.name} знает команду "ролл"')

obj = Dog('Willie', 6)
obj.get_info()
obj.sit()
obj.roll_over()
my_dog = Dog('Lucy', 3)
my_dog.get_info()
my_dog.sit()
my_dog.roll_over()

#Задание 3
class Human:

    # Статические поля
    default_name = 'Даниил'
    default_age = 0

    def __init__(self, name=default_name, age=default_age):
        self.name = name
        self.age = age
        self._money = 0
        self._house = None

    def info(self):
        print(f'Имя {self.name}')
        print(f'Возраст: {self.age}')
        print(f'Денег на счету: {self._money}')
        print(f'Дом: {self._house}')

    # Статический метод
    @staticmethod
    def default_info():
        print(f'Привет, меня зовут {Human.default_name}')
        print(f'Мне {Human.default_age}')

    def earn_money(self, amount):
        self._money += amount
        print(f'У вас {self._money} рублей на счету на данный момент')

    def buy_house(self, house, discount):
        price = house.final_price(discount)
        if self._money >= price:
            self._make_deal(house, price)
        else:
            print('Недостаточно денег на счету')

    # Приватный метод
    def _make_deal(self, house, price):
        self._money -= price
        self._house = house


class House:

    def __init__(self, area, price):
        self._area = area
        self._price = price

    def final_price(self, discount):
        final_price = self._price * (100 - discount) / 100
        print(f'Цена с учетом скидки: {final_price}')
        return final_price


class SmallHouse(House):

    default_area = 40

    def __init__(self, price):
        super().__init__(SmallHouse.default_area, price)
        
objects = Human('Саша', 19)
objects.info()

small_house = SmallHouse(8_500)

objects.buy_house(small_house, 5)

objects.earn_money(5_000)
objects.buy_house(small_house, 5)

objects.earn_money(20_000)
objects.buy_house(small_house, 5)
objects.info()

#Задание 4
from SlugConvertor import SlugConverter
if __name__ == '__main__':
    converter = SlugConverter()
    