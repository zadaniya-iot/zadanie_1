class CommonUtils:
    @staticmethod
    def translit_to_eng(s):
        translit_dict = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd',
            'е': 'e', 'ё': 'yo', 'ж': 'zh', 'з': 'z', 'и': 'i',
            'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
            'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
            'у': 'u', 'ф': 'f', 'х': 'kh', 'ц': 'ts', 'ч': 'ch',
            'ш': 'sh', 'щ': 'shch', 'ъ': '', 'ы': 'y', 'ь': '',
            'э': 'e', 'ю': 'yu', 'я': 'ya'
        }

        s = s.lower()
        slug1 = ''
        for char in s:
            if char in translit_dict:
                slug1 += translit_dict[char]
            else:
                slug1 += char

        return slug1

    @staticmethod
    def save_to_file(slug, data):
        with open(slug, 'w') as file:
            for item in data:
                file.write(item + '\n')

