#Задание 1
#Можно использовать библиотеку numpy, которая позволяет создавать многомерные массивы
import numpy as np
a = np.array([[1, 1], [1, 1]])

#Задание 2
import numpy as np
a = [1, 2, 3]
b = np.array([1, 2, 3])
print(a + a, b + b) # а в нашем случае является списком, и его сложение будет являтся конкатанацией, в случае numpy будет суммироваться соответствующий элементы

#Задание 3
import numpy as np
a = [1, 2, 3, 4, 5, 6]
b = [10, 11, 12, 13, 14, 15]
x = np.min((np.max(a), np.min(b))) #ответ: np.max(a) = 6, np.min(b) = 10, np.min(np.max(a), np.min(b)) = 6

#Задание 4
import numpy as np
A = np.array([[10, 1], [20, 2]])
x = np.sum(A, axis = 0) #axis - необязательный атрибут, который позволяет складывать строки, либо столбцы в многомерном массиве. Если axis = 0, то складываются строки, если 1 - складываются столбцы
print(x)# axis = 0 => ответ: [30, 3]

#Задание 5
I = np.eye(2) # создается единичная двумерная матрица
A = np.array([[0, 1], [1, 0]])
x = np.sum( np.dot(A, I) ) #матричное произведение
y = np.sum( A * I ) #скалярное произведение векторов
print(x, y) #ответ: 2.0 0.0

#Задание 6
import numpy as np
def nonzero(A):
    for i in range(len(A)):
        for j in range(len(A[0])):
            if A[i][j] != 0:
                return (i, j)
A = np.zeros((100,100))
A [56] [71] = 1
result = nonzero(A)
print(result)

#Задание 7
x = list(map(int, input().split()))

indices = []

for i in range(len(x)):
    while len(indices) < 3:
        s = min(x)
        indices.append(x.index(s))
        x.remove(s)
print(indices)

#Задание 8
import numpy as np

def closest(atoms, v, r):

    coordinates = np.array(atoms)
    v_coordinates = coordinates[v]
    distances = np.linalg.norm(coordinates - v_coordinates, axis=1)
    k = np.sum(distances <= r)

    return k-1
    
r = 2.5
v = 1
vecs = np.array(
 [
 [1.0, 0.0, 2.0],
 [-1.0, 0.5, 2.0],
 [-2.0, 1.5, 0.0],
 [2.5, -1.2, -0.5],
 [1.5, 0.2, -0.2]
 ]
)
result = closest(vecs, v, r)
print(result)


#Задание 2.1
import matplotlib.pyplot as plt
import numpy as np


x = np.linspace(-10, 10, 1000)

plt.figure(figsize=(8,3))
plt.grid(lw=0.5, ls='--')
plt.plot(x,(np.sin(2 * x) ** 2) * np.exp((x + 2) / 10) ** 2, lw = 4.0, color='red')
plt.plot(x,(np.sin(2 * x) ** 2) * np.exp((x + 2) / 10) ** 2, lw = 5.0, color='black', zorder=0)
plt.show()

#Задание 2.2
import matplotlib.pyplot as plt
import numpy as np
from math import pi

x = np.linspace(-3*pi, 3*pi, 250)
y = np.sin(x) + np.random.normal(1, 4, 250) * 20


plt.figure(figsize=(8,3))
plt.scatter(x, y_noise, s=300, marker='s', c='violet', lw=2, edgecolor='black', hatch='**')
plt.title(label='$sin(x)$ with random noise', fontsize=20)
plt.xlabel('x range', fontsize=18)
plt.ylabel('y range', fontsize=18)
plt.tick_params(labelsize=16)
plt.xticks(
    ticks=np.arange(-10, 11, 2)
)
4
plt.yticks(
    ticks=np.arange(-1.5, 2, 0.5),
    labels=['Учаев', 'Даниил', 'Компьютерная', 'Безопасность', 'Второй', 'Курс', 'Обучения'][::-1]
)

plt.show()

#Задание 2.2 Пример 1
import matplotlib.pyplot as plt
import numpy as np

y = np.random.standard_cauchy(10000000)

plt.hist(y, range=(0, 5), bins=50, density=True)
plt.show()

#Задание 2.2 Пример 2
import matplotlib.pyplot as plt
import numpy as np
from math import pi

x = np.linspace(-5*pi, 5*pi, 10000000)
y1 = np.random.normal(-10, 7, 10000000)
y2 = np.random.normal(10, 5, 10000000)
y3 = np.random.normal(25, 10, 15000000)
y = np.concatenate([y1, y2, y3]) 

plt.hist(y, bins=100, density=True)
plt.show()

#Задание 2.3
import matplotlib.pyplot as plt
import numpy as np

r = np.random.rand(150) * 2
theta = np.random.rand(150) * 360

plt.figure(figsize=(6, 6))
plt.axes(projection='polar')
plt.scatter(theta, r, s=400*r**2, c=theta, cmap='hsv', alpha=0.8)

plt.show()

#Задание 2.5
import matplotlib.pyplot as plt
import numpy as np

counts = [17, 3]
plt.figure(figsize=(5, 5))
plt.pie(counts, colors=['royalblue', 'gold'], labels=['Dogs', 'Cats'], startangle=120, autopct='%.3f%%')

plt.show()

#Задание 2.4
import matplotlib.pyplot as plt
import numpy as np
import random

x = np.linspace(-5, 5, 40)
y = np.sin(x) + np.tan(2*(x-2))
yerr1 = [random.sample([-1, 1], 1)[0] for _ in range(40)]

plt.figure(figsize=(10, 5))
plt.errorbar(x, y, yerr=yerr1, ecolor='forestgreen', capsize=10, elinewidth=1.5)

plt.show()

#Задание 2.5
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-2 * np.pi, 2 * np.pi, 100)
y1 = np.sin(x)
y2 = np.sin(x / 2)
y3 = np.cos(x)
y4 = np.cos(x / 2)

fig, axs = plt.subplots(2, 2)
axs[0, 0].plot(x, y1)
axs[0, 0].set_title('sin(x)')
axs[0, 1].plot(x, y2)
axs[0, 1].set_title('sin(x/2)')
axs[1, 0].plot(x, y3)
axs[1, 0].set_title('cos(x)')
axs[1, 1].plot(x, y4)
axs[1, 1].set_title('cos(x/2)')

plt.tight_layout()

plt.show()

#Задание 2.6
import numpy as np
import matplotlib.pyplot as plt

def func(x, y):
 return (x - 1)**2 - (y + 2)**2

x = np.linspace(-20, 20, 100)
y = np.linspace(-20, 20, 100)
X, Y = np.meshgrid(x, y)
Z = func(X, Y)

fig, ax = plt.subplots(1, 1, figsize=(6, 5))
obj = ax.imshow(Z, cmap=plt.cm.seismic, vmin=-400, vmax=400)
color_bar = fig.colorbar(obj,  
                         ax = ax, 
                         extend = 'both') 

plt.show()

#Задание 2.7
import numpy as np
import matplotlib.pyplot as plt

def func(x, y):
 return -np.sqrt(x**2 + y**2)

x = np.linspace(-20, 20, 100)
y = np.linspace(-20, 20, 100)
X, Y = np.meshgrid(x, y)
I = func(X, Y)

fig, ax = plt.subplots(1, 1, figsize=(6, 5))
mapp = ax.pcolormesh(X, Y, I, cmap='inferno', shading='auto', 
edgecolor='face')
fig.colorbar(mapp)

plt.show()

#Задание 7
import numpy as np
import matplotlib.pyplot as plt

def func(x, y):
    return (x**4 - y**4)

x = np.linspace(-20, 20, 100)
y = np.linspace(-20, 20, 100)
X, Y = np.meshgrid(x, y)
Z = func(X, Y)

fig, ax = plt.subplots(1, 1, figsize=(8, 8), subplot_kw={'projection': "3d"})
ax.plot_surface(X, Y, Z / Z.max(), cmap=plt.cm.ocean_r)
ax.view_init(30, 60)

plt.show()

#Задание 8
import numpy as np
import matplotlib.pyplot as plt

def func(x, y):
    return ((1/4)*np.sin(1/2*x**2 - y)) - np.exp(-((x-5)**2 + (y-2)**2))

x = np.linspace(2, 8, 100)
y = np.linspace(0, 5, 100)
X, Y = np.meshgrid(x, y)
Z = func(X, Y)

fig, ax = plt.subplots(1, 1, figsize=(7, 8), subplot_kw={'projection': "3d"})
ax.plot_surface(X, Y, Z / Z.max(), cmap=plt.cm.ocean_r)
ax.view_init(30, 60)

plt.show()

#Задание 8
import numpy as np
import matplotlib.pyplot as plt

def func(x, y):
    return ((1/4)*np.sin(1/2*x**2 - y)) - np.exp(-((x-5)**2 + (y-2)**2))

x = np.linspace(2, 8, 100)
y = np.linspace(0, 5, 100)
X, Y = np.meshgrid(x, y)
Z = func(X, Y)

fig, ax = plt.subplots(1, 1, figsize=(7, 8), subplot_kw={'projection': "3d"})
ax.plot_surface(X, Y, Z / Z.max(), cmap=plt.cm.ocean_r)
ax.view_init(30, 60)

plt.show()

#Задание 9
import numpy as np
import matplotlib.pyplot as plt

def func(x, y):
    return ((np.sin(x**2 + y**2))/(x**2 + y**2))

x = np.linspace(-4, 4, 100)
y = np.linspace(-4, 4, 100)
X, Y = np.meshgrid(x, y)
Z = func(X, Y)

fig, ax = plt.subplots(1, 1, figsize=(7, 8), subplot_kw={'projection': "3d"})
ax.plot_surface(X, Y, Z / Z.max(), cmap=plt.cm.ocean_r)
ax.view_init(30, 60)

plt.show()

#Задание 10
import numpy as np
import matplotlib.pyplot as plt

def func(x, y):
    return ((np.sin(x**2 + y**2))/(x**2 + y**2))

x = np.linspace(-4, 4, 100)
y = np.linspace(-4, 4, 100)
X, Y = np.meshgrid(x, y)
Z = func(X, Y)

fig, ax = plt.subplots(1, 1, figsize=(7, 8), subplot_kw={'projection': "3d"})
ax.plot_surface(X, Y, Z / Z.max(), cmap=plt.cm.ocean_r)
ax.view_init(30, 60)

plt.show()

#Задание 12
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint


func = lambda y, t: t**2
dt=1e-3
t = np.arange(0,1,dt)
res = odeint(func, y0 = 0, t = t)

plt.figure(figsize=(5,4))
plt.plot(t, res)
plt.plot(t[::50], t[::50]**3/3, 'o')

plt.show()



